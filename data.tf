data "aws_iam_policy_document" "consumer" {
  statement {
    actions = [
      "kms:Decrypt"
    ]
    effect    = "Allow"
    resources = [aws_kms_key.queue.arn]
    sid       = "Decrypt"
  }

  statement {
    actions = [
      "sqs:ChangeMessageVisibility",
      "sqs:DeleteMessage",
      "sqs:ReceiveMessage"
    ]
    effect    = "Allow"
    resources = [aws_sqs_queue.default.arn]
    sid       = "ReceiveMessage"
  }
}

data "aws_iam_policy_document" "producer" {
  statement {
    actions = [
      "kms:Decrypt",
      "kms:Encrypt",
      "kms:GenerateDataKey*"
    ]
    effect    = "Allow"
    resources = [aws_kms_key.queue.arn]
    sid       = "Encrypt"
  }

  statement {
    actions = [
      "sqs:SendMessage"
    ]
    effect    = "Allow"
    resources = [aws_sqs_queue.default.arn]
    sid       = "SendMessage"
  }
}
